#include <iostream>
#include <esp_timer.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#define TAG "HX711"

#define HX711_Pin_DOut GPIO_NUM_11
#define HX711_Pin_Sck GPIO_NUM_10
#define NOP() asm volatile ("nop")

typedef enum {
    LSBFIRST = 1,    /*!< Use to signal not connected to S/W */
    MSBFIRST= 2,
/** @endcond */
} LSB;

unsigned long IRAM_ATTR micros()
{
    return (unsigned long)(esp_timer_get_time());
}

void IRAM_ATTR delayMicros(uint32_t us)
{
    uint32_t m = micros();
    if (us){
        uint32_t e = (m + us);
        if (m > e){ //overflow
            while (micros() > e){
                NOP();
            }
        }
        while (micros() < e){
            NOP();
        }
    }
}

class HX711{
public:
    gpio_num_t pin_DOut;
    gpio_num_t pin_Sck;
    int gain;
    long Offset;

    void init(gpio_num_t pin_DOut, gpio_num_t pin_Sck, int GAIN){
        gpio_reset_pin(pin_DOut);
        gpio_set_direction(pin_DOut,GPIO_MODE_INPUT);
        gpio_reset_pin(pin_Sck);
        gpio_set_direction(pin_Sck,GPIO_MODE_OUTPUT);
        this->pin_DOut = pin_DOut;
        this->pin_Sck = pin_Sck;
        switch (GAIN) {
            case 128:		// channel A, gain factor 128
                this->gain = 1;
                break;
            case 64:		// channel A, gain factor 64
                this->gain = 3;
                break;
            case 32:		// channel B, gain factor 32
                this->gain = 2;
                break;
        }
    }
    bool is_ready(void){
        return gpio_get_level(pin_DOut)==0;
    }
    void wait_ready(int delay_ms=0){
        while(!is_ready()){
            vTaskDelay(delay_ms/ portTICK_PERIOD_MS);
        }
    }
    uint8_t shiftInSlow(LSB bitOrder) {
        uint8_t value = 0;
        uint8_t i;

        for(i = 0; i < 8; ++i) {
            gpio_set_level(this->pin_Sck,1);
            vTaskDelay(1/ portTICK_PERIOD_MS);
            delayMicros(1);
            if(bitOrder == LSBFIRST)
                value |= gpio_get_level(pin_DOut) << i;
            else
                value |= gpio_get_level(pin_DOut) << (7 - i);
            gpio_set_level(pin_Sck, 0);
            delayMicros(1);
        }
        return value;
    }

    long read(void){
        wait_ready();

        unsigned long value=0;
        uint8_t data[3]={0};
        uint8_t filler = 0x00;
        portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
        portENTER_CRITICAL(&mux);

        data[2] = shiftInSlow( MSBFIRST);
        data[1] = shiftInSlow( MSBFIRST);
        data[0] = shiftInSlow( MSBFIRST);

        for (unsigned int i = 0; i < this->gain; i++) {
            gpio_set_level(this->pin_Sck, 1);
            delayMicros(1);
            gpio_set_level(this->pin_Sck, 0);
            delayMicros(1);
        }

        portEXIT_CRITICAL(&mux);
        if (data[2] & 0x80) {
            filler = 0xFF;
        } else {
            filler = 0x00;
        }
        value = ( static_cast<unsigned long>(filler) << 24
                  | static_cast<unsigned long>(data[2]) << 16
                  | static_cast<unsigned long>(data[1]) << 8
                  | static_cast<unsigned long>(data[0]) );

        return (static_cast<long>(value)) ;
    }

    long read_average(int times=10){
        long sum = 0;
        for (int i = 0; i < times;  i++){
            sum += this->read();
            vTaskDelay(10/ portTICK_PERIOD_MS);
        }
        return sum/times+this->Offset;
    }
    void get_Offset(void){
        long sum = 0;
        for (int i = 0; i < 10;  i++){
            sum += this->read();
            vTaskDelay(10/ portTICK_PERIOD_MS);
        }
        this->Offset = -sum/10;
    }
};



extern "C" void app_main(void)
{
    HX711 myHX711;
    myHX711.init(HX711_Pin_DOut,HX711_Pin_Sck,128);
    vTaskDelay(1000/ portTICK_PERIOD_MS);
    myHX711.get_Offset();
    while (1) {
        //ESP_LOGI(TAG, "Start...");
        vTaskDelay(100/ portTICK_PERIOD_MS);
        std::cout << "Weight: "<<myHX711.read_average(5)/100<<" g - ";
        //std::cout << "Offset: "<<myHX711.Offset;
        std::cout<<std::endl;
    }

}
